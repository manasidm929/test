<%-- 
    Document   : first
    Created on : 16 Apr, 2020, 11:31:10 AM
    Author     : Manasi Malkar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>

        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">

        <style>
            * {
                box-sizing: border-box;
            }


            body {
                font-family: Arial, Helvetica, sans-serif;
                margin: 0;
            }
            .container1 {
                position: relative;
                max-width: 1500px;
                margin: 0 auto;

            }

            .container1 img {vertical-align: top;}


            /* Navbar links */
            .topnav a {
                float: top;
                color: grey;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 20px;
                font-weight: bold
            }

            .topnav a:hover {
                background-color: #696969;
                color: black;

            }
            .topnav a.active {

                color: #4CAF50;
                text-decoration: underline;
            }
            .topnav .icon {
                display: none;
            }

            @media screen and (max-width: 600px) {
                .topnav a:not(:first-child) {display: none;}
                .topnav a.icon {
                    float: right;
                    display: block;
                }
            }

            @media screen and (max-width: 600px) {
                .topnav.responsive {position: relative;}
                .topnav.responsive .icon {
                    position: absolute;
                    right: 0;
                    top: 0;
                }
                .topnav.responsive a {
                    float: none;
                    display: block;
                    text-align: left;
                }
            }
            .topnav1 a {
                float: top;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 17px;
            }
            .topnav1 a:hover {

                color: black;
            }

            .topnav1 a.active {
                color: #4CAF50;
                text-decoration: underline;
            }


            .topnav1 .search-container {
                float: right;
            }

            .topnav1 input[type=text] {
                padding: 6px;
                margin-top: 8px;
                font-size: 17px;
                border: none;
            }

            .topnav1 .search-container button {
                float: right;
                padding: 6px 10px;
                margin-top: 8px;
                margin-right: 16px;
                background: #ddd;
                font-size: 17px;
                border: none;
                cursor: pointer;
            }

            .topnav1 .search-container button:hover {
                background: #ccc;
            }

            @media screen and (max-width: 600px) {
                .topnav1 .search-container {
                    float: none;
                }
                .topnav1 a, .topnav1 input[type=text], .topnav1 .search-container button {
                    float: none;
                    display: block;
                    text-align: left;
                    width: 100%;
                    margin: 0;
                    padding: 14px;
                }
                .topnav1 input[type=text] {
                    border: 1px solid #ccc;  
                }
            }
            /* Header/Logo Title */
            .header {

                text-align: center;

                color: white;
                font-size: 30px;

                background-color: black;
            }
            /* Float four columns side by side */
            .column {
                float: left;
                width: 25%;
                padding: 0 10px;
            }

            /* Remove extra left and right margins, due to padding */
            .row {margin: 0 -5px;}

            /* Clear floats after the columns */
            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Responsive columns */
            @media screen and (max-width: 600px) {
                .column {
                    width: 100%;
                    display: block;
                    margin-bottom: 20px;
                }
            }

            /* Style the counter cards */
            .card {
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
                height: 660px;
                width: 300px;
                text-align: center;
                background-color: #f1f1f1;
                margin-top: 20px;

            }
            .content {padding:20px;
            }

            .btn-success{
                margin-top: 30px;
                color: white;
                height: 50px;
                width: 180px;
            }
            hr.new1 {
                border: 1px solid black;
                margin-top: 150px;
                margin-left: 4px;
                margin-right: 4px;

            }
            hr.new2{

                height: 4px;
                opacity: 0.2;
                background-color: #ffffff;
            }
            section{
                background-color: black;
            }

            .icontainer .ibtn:hover {
                background-color: Black;
            }
            .footers a {color:#696969;}
            .footers p {color:#696969;}
            .footers ul {line-height:30px;}
            #social-fb:hover {
                color: #3B5998;
                transition:all .001s;
            }
            #social-tw:hover {
                color: #4099FF;
                transition:all .001s;
            }
            #social-gp:hover {
                color: #d34836;
                transition:all .001s;
            }
            #social-em:hover {
                color: #f39c12;
                transition:all .001s;
            }

            .btn-default{
                width: 160px;
                height: 50px;
            }

            .container1 .content {
                position: absolute;
                top: -400px;
                height: 200px;

                background-color: white;
                opacity: 0.8;
                color: #f1f1f1;
                width: 102%;
                padding: 20px;
                margin-left: -10px;
            }
            .topnav{
                margin-left: 80px;
                margin-top: 50px;
            }

            .logo{

                margin-top: 10px;
                height: 140px;
                width: 140px;
                margin-left: 40px;
                float: left;


            }
            .login{
                background: black;
                border-radius: 10px;
                height: 60px;
                width: 160px;
                border-color: black;
            }

            .form-group {
                margin:0;
                padding:20px ;


            }

            .form-control {
                padding: 0px 10px 0 20px;
                margin-top: 10px;
                color: #333;
                font-size: 28px;
                font-weight: 500;

                -webkit-box-shadow: none;
                box-shadow: none;

                height: 46px;
                border-radius: 50px 0  0 50px !important;

            }
            .form-control :focus {
                -webkit-box-shadow: none;
                box-shadow: none;
                border-color: transparent;

            }

            #searchbtn
            { border:0;
              padding: 0px 10px;
              margin-top: 10px;
              margin-left: -10px;
              color: #fff;
              background:#888;
              font-size: 27px;
              font-weight: 500;

              border-left: -5px;
              -webkit-box-shadow: none;
              box-shadow: none;

              height: 45px;
              float: right;
              border-radius: 0 50px 50px 0 !important;
            }
            .progress-title{
                font-size: 16px;
                font-weight: 700;
                color: #011627;
                margin: 0 0 20px;
            }
            .progress {
                height: 10px;
                background: white;
                border-radius: 5px;
                box-shadow: none;
                overflow: visible;
                margin-top: 60px;
            }

            .progress .progress-bar {
                box-shadow: none;
                position: relative;
                -webkit-animation: animate-positive 2s;
                animation: animate-positive 2s;
                border-bottom-left-radius: 5px;
                border-top-left-radius: 5px;
            }


            .progress .progress-bar::after {
                content: "";
                border: 9px solid transparent;
                border-bottom: 15px solid transparent;
                position: absolute;
                top: -19.5px;
                right: -8px;
                transform: rotate(180deg);
            }

            .progress .progress-value {
                font-size: 15px;
                font-weight: bold;
                color: #000;
                background-color: green;
                color: white;
                position: absolute;
                top: -35px;
                right: 0px;
                width: 50px;
                margin-right: -23px;
            }


            .progress.pink .progress-bar:after{
                border-bottom-color: #ff4b7d;
            }
            .progress.green .progress-bar:after{
                border-bottom-color: green;
            }
            .progress.yellow .progress-bar:after{
                border-bottom-color: #e8d324;
            }
            .progress.blue .progress-bar:after{
                border-bottom-color: #3485ef;
            }
            @-webkit-keyframes animate-positive{
                0% { width: 0; }
            }
            @keyframes animate-positive{
                0% { width: 0; }
            }

            .icontainer .ibtn {
                position: absolute;
                margin-top: -128px;
                left: 69%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                background-color: white;
                color: black;
                font-size: 16px;
                padding: 12px 24px;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                text-align: center;
            }




        </style>
    </head>
    <body>


        <div class="header">
            <img src="images/rock-staar-cGjdNsu0H14-unsplash.jpg" alt="Notebook" style="height: 400px;width:100%;opacity: 0.4">

            <div class="container1">

                <div class="content">
                    <img src="images/BitHelp Logo@3x.png" alt="logo" class="logo">
                    <div class="topnav" id="myTopnav">

                        <a href="#home">Home</a>
                        <a href="#news" class="active">Causes</a>
                        <a href="#news">Stories</a>
                        <a href="#about">About us</a>
                        <a href="#contact">Blog</a>
                        <a href="#contact">Contact</a>
                        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                            <i class="fa fa-bars"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="login" style="color: white; font-family: sans-serif;
                                font-size: 15px;float: 80px;">Login/Register</button>

                    </div>

                    <h1 style="float: left;margin-top: 130px;opacity: 50;font-size: 60px;color: white;font-weight: bold;font-family: sans-serif"> Causes </h1>
                </div>

            </div>
        </div>







        <div class="content" style="margin-left: 250px">
            <div class="topnav1" style="margin-left: 5px;margin-top: 40px;">

                <a href="#home" class="active">All</a>
                <a href="#news">Awareness</a>
                <a href="#news">Children aid</a>
                <a href="#about">Education</a>
                <a href="#contact">Food</a>
                <a href="#contact">Health</a>
                <a href="#contact">Disaster Management</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="search-container">
                    <form action="/action_page.php">
                        <input type="text" placeholder="Search.." name="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>

            </div>
            &nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;
            &nbsp;


            <div class="row">
                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200" 
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                            <div style="float:right;font-size: 10px;margin-top: 25px;font-weight: bold;color: darkcyan;
                                 margin-right: 4px">15 supporters</div>

                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200" 
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                            <div style="float:right;font-size: 10px;margin-top: 25px;font-weight: bold;color: darkcyan;
                                 margin-right: 4px">15 supporters</div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200"
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                            <div style="float:right;font-size: 10px;margin-top: 25px;font-weight: bold;color: darkcyan;
                                 margin-right: 4px">15 supporters</div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

            </div>

            <h2>                            </h2>
            <div class="row">
                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200" 
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                            <div style="float:right;font-size: 10px;margin-top: 25px;font-weight: bold;color: darkcyan;
                                 margin-right: 4px">15 supporters</div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200" 
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                            <div style="float:right;font-size: 10px;margin-top: 25px;font-weight: bold;color: darkcyan;
                                 margin-right: 4px">15 supporters</div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200"
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                            <div style="float:right;font-size: 10px;margin-top: 25px;font-weight: bold;color: darkcyan;
                                 margin-right: 4px">15 supporters</div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>


            </div>

            <h2>                             </h2>
            <div class="row">
                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200"
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200" 
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>
                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

                <div class="column">
                    <div class="card">
                        <img src="images/NGO Img 1.png" class="img-rounded" alt="Cinque Terre" width="300" height="200" 
                             style="margin-top: 2px;border-bottom-right-radius: 0px;border-bottom-left-radius: 0px;"> 
                        <div>
                            <div class="progress green">
                                <div class="progress-bar" style="width:75%; background:#5fad56;">
                                    <div class="progress-value">75%</div>
                                </div>
                            </div>

                            <div><div style="float:left; margin-left: 4px ;margin-top: -5px">Raised ₹8000/ month</div>
                                <div style="float:right;margin-top: -5px;margin-right: 4px">Goal ₹4000/month</div></div>

                        </div>
                        <div><h5 style="font-family: sans-serif; font-weight: bold; color: black;margin-top: -70px; font-size: x-large;margin-left: 5px;float: left">Nagpur Orphanage</h5>

                            <h6 style="float: right;margin-right: 10px;font-weight: bold;font-size: 10px; margin-top: -25px;">Project Funding Period   &nbsp; &nbsp;    <i style='font-size:10px' class='far'>&#xf073;</i>&nbsp;36 months</h6>
                            <hr class="new1">

                            <h6 style="font-size: 13px;margin-left: 4px;margin-right: 4px;text-align: justify">Once we obtain the LWG certification,nobody 
                                will ask about the quality of our products as everything would be produced following the international standards of compliance,
                                "said Saiful Islam,president of the Leathergoods and Footwear Manufactures & Exporters Association of Bangladesh.</h6></div>

                        <button type="button" class="btn btn-success">Read More</button>
                    </div>
                </div>

            </div>

        </div>


        <div class="icontainer">
            <img src="images/donorPage.jpg" alt="abc" style="height: 400px;width:100%;">


            <button class="ibtn" >Join Us Now</button>
        </div>

        <section class="footers bg-light pt-5 pb-3">
            <div class="container pt-5" style="padding: 50px">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 footers-one">
                        <div class="footers-logo">
                            <img src="images/BitHelp Logo@3x.png" alt="logo" style="margin-top: 1px;height: 100px;width: 100px;margin-left: 10px;float: contour">
                        </div>
                        <div class="footers-info mt-3">
                            <p>Core values are the fundamental beliefs of a person or organization. The core values are the guiding prin ples that dictate behavior and action suas labore saperet has there any quote for write lorem percit latineu</p>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2 footers-two" style="margin-left: 20px;margin-top: 40px;">
                        <h5 style="font-family: sans-serif; font-weight: bold; color: white">About Us </h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Who we are</a></li>
                            <li><a href="#">How we work</a></li>
                            <li><a href="#">Financial</a></li>

                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2 footers-three" style="margin-left: 20px;margin-top: 40px;">
                        <h5 style="font-family: sans-serif; font-weight: bold; color: white">Your Help </h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Leave a legacy</a></li>
                            <li><a href="#">Monthly Giving</a></li>

                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2 footers-four" style="margin-left: 20px;margin-top: 40px;">
                        <h5 style="font-family: sans-serif; font-weight: bold; color: white">Contact Us </h5>
                        <ul class="list-unstyled">

                            <li><a href="about.html">________</a></li>
                            <li><a href="about.html">_______</a></li>
                            <li><a href="about.html">__________</a></li>
                        </ul>

                    </div>


                </div>
                <div class="social-icons" style="margin-left: 10px;"> 

                    <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-2x social"></i></a>
                    <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-2x social"></i></a>
                    <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-2x social"></i></a>
                    <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-2x social"></i></a>


                    <div style="float:right">

                        <form role="search" >
                            <div class="input-group" style='
                                 width: 330px;
                                 margin-top: -28px;
                                 margin-right: -38px; '>
                                <div><input class="form-control" style="border-color: black;border-right: 0px" placeholder="Email address . . ." name="srch-term" id="ed-srch-term" type="email"></div>
                                <div class="input-group-btn"> 
                                    <button type="submit" style="background-color: darkcyan;border-color: black; border-left: 0px" id="searchbtn">Submit</button>
                                </div> 
                            </div>
                        </form>
                    </div>



                </div>

            </div>
        </section>
        <section class="disclaimer bg-light border">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 py-2">

                    </div>
                </div>
            </div>
        </section>
        <section class="copyright border">
            <div class="container">
                <hr class="new2">
                <div class="row text-center">
                    <div class="col-md-12 pt-3">
                        <p class="text-muted">Copyrights © 2020 BitHelp Foundation. All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;Privacy&nbsp;&nbsp;&nbsp;&nbsp;Sitemap&nbsp;&nbsp;&nbsp;&nbsp;Terms of Use</p>
                    </div>
                </div>
            </div>
        </section>


        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script> 





    </body>
</html>

