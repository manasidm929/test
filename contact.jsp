<%-- 
    Document   : contact
    Created on : 18 Apr, 2020, 5:12:53 PM
    Author     : Manasi Malkar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src='https://kit.fontawesome.com/a076d05399.js'></script>
        <link rel="stylesheet"
              href= 
              "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">

        <style>
            * {
                box-sizing: border-box;
            }

            /*body {
              font-family: Arial, Helvetica, sans-serif;
            }*/

            body {
                font-family: Arial, Helvetica, sans-serif;
                margin: 0;
            }
            .container1 {
                position: relative;
                max-width: 1500px;
                margin: 0 auto;

            }

            .container1 img {vertical-align: top;}


            /* Navbar links */
            .topnav a {
                float: top;
                color: grey;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
                font-size: 20px;
                font-weight: bold;
            }

            .topnav a:hover {
                background-color: #696969;
                color: black;

            }
            .topnav a.active {
                /*background-color: #4CAF50;*/
                color: #4CAF50;
                text-decoration: underline;
            }
            .topnav .icon {
                display: none;
            }

            @media screen and (max-width: 600px) {
                .topnav a:not(:first-child) {display: none;}
                .topnav a.icon {
                    float: right;
                    display: block;
                }
            }

            @media screen and (max-width: 600px) {
                .topnav.responsive {position: relative;}
                .topnav.responsive .icon {
                    position: absolute;
                    right: 0;
                    top: 0;
                }
                .topnav.responsive a {
                    float: none;
                    display: block;
                    text-align: left;
                }
            }
            .header {

                text-align: center;
                /*  background: #1abc9c;*/
                color: white;
                font-size: 30px;
                /*  background-image: url("images/meow.jpg");*/
                background-color: black;
            }
            hr.new2{

                height: 4px;
                opacity: 0.2;
                background-color: #ffffff;
            }
            section{
                background-color: black;
            }
            .icontainer .ibtn {
                position: absolute;
                margin-top: -128px;
                left: 69%;
                transform: translate(-50%, -50%);
                -ms-transform: translate(-50%, -50%);
                background-color: white;
                color: black;
                font-size: 16px;
                padding: 12px 24px;
                border: none;
                cursor: pointer;
                border-radius: 5px;
                text-align: center;
            }

            .icontainer .ibtn:hover {
                background-color: Black;
            }
            .footers a {color:#696969;}
            .footers p {color:#696969;}
            .footers ul {line-height:30px;}
            #social-fb:hover {
                color: #3B5998;
                transition:all .001s;
            }
            #social-tw:hover {
                color: #4099FF;
                transition:all .001s;
            }
            #social-gp:hover {
                color: #d34836;
                transition:all .001s;
            }
            #social-em:hover {
                color: #f39c12;
                transition:all .001s;
            }
            .container1 .content {
                position: absolute;
                top: -400px;
                height: 200px;
                /*    background: rgb(0, 0, 0);
                    background: rgba(0, 0, 0, 0.8);*/
                background-color: white;
                opacity: 0.8;
                color: #f1f1f1;
                width: 103%;
                padding: 20px;
                margin-left: -18px;
            }
            .topnav{
                margin-left: 80px;
                margin-top: 50px;
            }

            .logo{

                margin-top: 10px;
                height: 140px;
                width: 140px;
                margin-left: 40px;
                float: left;


            }
            .login{
                background: black;
                border-radius: 10px;
                height: 60px;
                width: 160px;
                border-color: black;
            }
            .form-group {
                margin:0;
                padding:20px ;


            }

            .form-control {
                padding: 0px 10px 0 20px;
                margin-top: 10px;
                color: #333;
                font-size: 28px;
                font-weight: 500;

                -webkit-box-shadow: none;
                box-shadow: none;

                height: 46px;
                border-radius: 50px 0  0 50px !important;

            }
            .form-control :focus {
                -webkit-box-shadow: none;
                box-shadow: none;
                border-color: transparent;

            }
            #searchbtn
            { border:0;
              padding: 0px 10px;
              margin-top: 10px;
              margin-left: -10px;
              color: #fff;
              background:#888;
              font-size: 27px;
              font-weight: 500;

              border-left: -5px;
              -webkit-box-shadow: none;
              box-shadow: none;

              height: 45px;
              float: right;
              border-radius: 0 50px 50px 0 !important;
            }
            .rock-staar-cGjdNsu0H14-unsplash{
                opacity: 0.2;
            }

            #responsive-form{
                max-width:600px /*-- change this to get your desired form width --*/;
                margin:0 auto;
                width:100%;
            }
            .form-row{
                width: 100%;
            }
            .column-half, .column-full{
                float: left;
                position: relative;
                padding: 0.65rem;
                width:100%;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box
            }
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }

            /**---------------- Media query ----------------**/
            @media only screen and (min-width: 48em) { 
                .column-half{
                    width: 50%;
                }
            }

            .wpcf7 input[type="text"], .wpcf7 input[type="email"], .wpcf7 textarea {
                width: 100%;
                padding: 8px;
                border: 1px solid #ccc;
                border-radius: 3px;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box
            }
            .wpcf7 input[type="text"]:focus{
                background: #fff;
            }
            .wpcf7-submit{
                float: right;
                background: #CA0002;
                color: #fff;
                text-transform: uppercase;
                border: none;
                padding: 8px 20px;
                cursor: pointer;
            }
            .wpcf7-submit:hover{
                background: #ff0000;
            }
            span.wpcf7-not-valid-tip{
                text-shadow: none;
                font-size: 12px;
                color: #fff;
                background: #ff0000;
                padding: 5px;
            }
            div.wpcf7-validation-errors { 
                text-shadow: none;
                border: transparent;
                background: #f9cd00;
                padding: 5px;
                color: #9C6533;
                text-align: center;
                margin: 0;
                font-size: 12px;
            }
            div.wpcf7-mail-sent-ok{
                text-align: center;
                text-shadow: none;
                padding: 5px;
                font-size: 12px;
                background: #59a80f;
                border-color: #59a80f;
                color: #fff;
                margin: 0;
            }
            #map { 
                height: 400px; 
                width: 100%; 
            } 
            .input-icons i { 
                position: absolute; 
            } 

            .input-icons { 
                width: 100%; 
                margin-bottom: 10px; 
            } 

            .icon {
                padding: 30px;
                color: black;
                height: 50px;
                width: 50px;
                float: left;
                text-align: center;
            }
            .icon1 {
                padding: 15px;
                color: black;
                height: 50px;
                width: 50px;
                float: left;
                text-align: center;
            }
            .icon2 {
                padding: 30px;
                color: darkcyan;

                float: left;
                text-align: center;
            }
            textarea{
                text-align: center;
            }

            .input-field { 
                width: 100%; 
                padding: 10px; 
                text-align: center; 
            } 

            h2 { 
                color: green; 
            } 
            .in-Touch {
                width: 171px;
                height: 55px;
                font-family: AvenirNextLTPro;
                font-size: 40px;
                font-weight: bold;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.2;
                letter-spacing: 1px;
                /*  text-align: left;*/
                margin-left: 280px;
                margin-top: -20px;
                color: #36404c;
                text-decoration: darkcyan;

            }
            .line1 {
                width: 30px;
                height: 3px;
                background-color: #29af8a;
                margin-left: 280px;
                margin-top: -14px;
            }
            .Get {
                padding: 20px;
                width: 32px;
                height: 25px;
                font-family: AvenirNextLTPro;
                font-size: 18px;
                font-weight: 500;
                font-stretch: normal;
                font-style: normal;
                line-height: 1.07;
                letter-spacing: 1.12px;
                /*  text-align: left;*/
                margin-left: 260px;

                color: #36404c;
            }

            .send{
                height: 40px;
                width: 550px;
                background-color: darkcyan;
                text-align: center;
                border: 0px;
                margin-bottom: 20px;
                color: white;
                font-weight: bold;
                border-radius: 5px;
            }
            .columni {
                float: left;
                width: 33.33%;
                padding: 5px;
            }

            /* Clearfix (clear floats) */
            .rowi::after {
                content: "";
                clear: both;
                display: table;
            }
        </style>
    </head>
    <body>

        <div class="header">
            <img src="images/rock-staar-cGjdNsu0H14-unsplash.jpg" alt="Notebook" style="height: 400px;width:100%;opacity: 0.4">

            <div class="container1">

                <div class="content">
                    <img src="images/BitHelp Logo@3x.png" alt="logo" class="logo">
                    <div class="topnav" id="myTopnav">

                        <a href="#home">Home</a>
                        <a href="#news" class="active">Causes</a>
                        <a href="#news">Stories</a>
                        <a href="#about">About us</a>
                        <a href="#contact">Blog</a>
                        <a href="#contact">Contact</a>
                        <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                            <i class="fa fa-bars"></i>
                        </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="login" style="color: white; font-family: sans-serif;
                                font-size: 15px;float: 80px;">Login/Register</button>

                    </div>

                    <h1 style="float: left;margin-top: 130px;opacity: 50;font-size: 60px;color: white;font-weight: bold;font-family: sans-serif"> Contact </h1>
                </div>


            </div>
        </div>

        <div class="rowi">
            <div class="columni">
                <div><i class="fa fa-phone icon2 fa-4x" style="margin-left: 300px;"></i></div>

            </div>
            <div class="columni">
                <i class="fa fa-envelope icon2 fa-4x" style="margin-left: 180px;"></i> 
            </div>
            <div class="columni">
                <i class="fa fa-map-marker icon2 fa-4x" aria-hidden="true" style="margin-left: 110px;"></i>
            </div>
        </div>
        <div class="rowi">
            <div class="columni">
                <div style=""><p style="margin-left: 230px;text-align: center;margin-top: -20px;">Phone<br>+(91)1234567890</p></div>
                <!--<div>1234567890</div>-->
            </div>
            <div class="columni">
                <div style=""><p style="margin-left: -15px;text-align: center;margin-top: -20px;">Email<br>info@bithelp.com</p></div>
            </div>
            <div class="columni">
                <div style=""><p style="margin-left: -180px;text-align: center;margin-top: -20px;">Location<br>Pune,MH,India-411 051</p></div>
            </div>
        </div>
        <div  style="background-color: lightgray;">



            <div>
                <p class="Get">Get</p>
                <p class="in-Touch">in Touch</p>
                <p class="line1"></p>
            </div>

            <form action="//submit.form" id="ContactUs100" method="post" onsubmit="return ValidateForm(this);">
                <div id="responsive-form" class="clearfix">

                    <div class="form-row">
                        <div class="column-half">
                            <!--    <div class="column-half">First Name<input name="Name" placeholder="First Name" type="text" maxlength="60" style="width:250px;" /></div>
                            <div class="column-half">Last Name <input name="Name" placeholder="Last Name" type="text" maxlength="60" style="width:250px;" /></div>-->
                            <div class="input-icons"> 
                                <i class="fa fa-phone icon" aria-hidden="true"></i>
                                <input class="input-field"
                                       type="number"
                                       name="PhoneNumber"
                                       placeholder="Phone no"
                                       maxlength="10" style="width:250px;margin-top: 15px;" > 
                            </div> 
                            <div class="input-icons"> 
                                <i class="fa fa-user icon"> 
                                </i> 
                                <input class="input-field"
                                       type="text"
                                       name="Name"
                                       placeholder="Full Name"
                                       maxlength="80" style="width:250px;margin-top: 15px;" > 
                            </div> 


                        </div>
                    </div>

                    <div class="form-row">
                        <!--    <div class="column-half">Email <input name="FromEmailAddress" type="email" placeholder="Email Address" maxlength="90" style="width:250px;" /></div>
                        <div class="column-half">Phone <input name="PhoneNumber" placeholder="Phone no" type="text" maxlength="43" style="width:250px;" /></div>-->

                        <div class="input-icons"> 
                            <i class="fa fa-envelope icon"> 
                            </i> 
                            <input class="input-field"
                                   type="email"
                                   name="FromEmailAddress"
                                   placeholder="Email Address"
                                   maxlength="90" style="width:250px;margin-top: 19px;" > 
                        </div> 
                        <div class="input-icons"> 
                            <i class="fa fa-tag icon"> 
                            </i> 
                            <input class="input-field"
                                   type="text"

                                   placeholder="Topic"
                                   maxlength="90" style="width:250px;margin-top: 19px;" > 
                        </div> 



                    </div>





                    <div class="form-row">

                        <div class="column-full">
                            <div class="input-icons"> 
                                <i class="fa fa-comment icon1"></i>
                                <p><textarea name="Comments" placeholder="Message" rows="7" cols="40" style="width:350px;"></textarea></p> 
                            </div> 
                            <!--                    <p><textarea name="Comments" rows="7" cols="40" style="width:350px;"></textarea></p>-->
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="column-full">
                            <!--                        <input name="skip_Submit" placeholder="SEND" type="submit" value="Submit"  />-->
                            <button class="send" type="submit" form="form1" value="Submit">SEND</button>

                        </div>
                        <script src="#" type="text/javascript"></script>
                    </div>

                </div><!--end responsive-form-->
            </form>     
        </div>

        <div id="map"></div> 
        <script>
                function initMap() {
                    var uluru = {lat: 18.51672618, lng: 73.856255};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        zoom: 4,
                        center: uluru
                    });
                    var marker = new google.maps.Marker({
                        position: uluru,
                        map: map
                    });
                }
        </script> 
        <script async defer 
                src= 
                "https://maps.googleapis.com/maps/api/js?key= 
                YOUR_KEY&callback=initMap">
        </script> 
















        <div class="icontainer">
            <img src="images/donorPage.jpg" alt="abc" style="height: 400px;width:100%;">


            <button class="ibtn" >Join Us Now</button>
        </div>

        <section class="footers bg-light pt-5 pb-3">
            <div class="container pt-5" style="padding: 50px">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4 footers-one">
                        <div class="footers-logo">
                            <img src="images/BitHelp Logo@3x.png" alt="logo" style="margin-top: 1px;height: 100px;width: 100px;margin-left: 10px;float: contour">
                        </div>
                        <div class="footers-info mt-3">
                            <p>Core values are the fundamental beliefs of a person or organization. The core values are the guiding prin ples that dictate behavior and action suas labore saperet has there any quote for write lorem percit latineu</p>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2 footers-two" style="margin-left: 20px;margin-top: 40px;">
                        <h5 style="font-family: sans-serif; font-weight: bold; color: white">About Us </h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Who we are</a></li>
                            <li><a href="#">How we work</a></li>
                            <li><a href="#">Financial</a></li>

                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2 footers-three" style="margin-left: 20px;margin-top: 40px;">
                        <h5 style="font-family: sans-serif; font-weight: bold; color: white">Your Help </h5>
                        <ul class="list-unstyled">
                            <li><a href="#">Leave a legacy</a></li>
                            <li><a href="#">Monthly Giving</a></li>

                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-2 footers-four" style="margin-left: 20px;margin-top: 40px;">
                        <h5 style="font-family: sans-serif; font-weight: bold; color: white">Contact Us </h5>
                        <ul class="list-unstyled">

                            <li><a href="about.html">________</a></li>
                            <li><a href="about.html">_______</a></li>
                            <li><a href="about.html">__________</a></li>
                        </ul>

                    </div>


                </div>
                <div class="social-icons" style="margin-left: 10px;"> 

                    <a href="https://www.facebook.com/"><i id="social-fb" class="fa fa-facebook-square fa-2x social"></i></a>
                    <a href="https://twitter.com/"><i id="social-tw" class="fa fa-twitter-square fa-2x social"></i></a>
                    <a href="https://plus.google.com/"><i id="social-gp" class="fa fa-google-plus-square fa-2x social"></i></a>
                    <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-2x social"></i></a>


                    <div style="float:right">

                        <form role="search" >
                            <div class="input-group" style='
                                 width: 330px;
                                 margin-top: -28px;
                                 margin-right: -38px; '>
                                <div><input class="form-control" style="border-color: black;border-right: 0px" placeholder="Email address . . ." name="srch-term" id="ed-srch-term" type="email"></div>
                                <div class="input-group-btn"> 
                                    <button type="submit" style="background-color: darkcyan;border-color: black; border-left: 0px" id="searchbtn">Submit</button>
                                </div> 
                            </div>
                        </form>
                    </div>



                </div>

            </div>
        </section>
        <section class="disclaimer bg-light border">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12 py-2">

                    </div>
                </div>
            </div>
        </section>
        <section class="copyright border">
            <div class="container">
                <hr class="new2">
                <div class="row text-center">
                    <div class="col-md-12 pt-3">
                        <p class="text-muted">Copyrights © 2020 BitHelp Foundation. All rights reserved. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;Privacy&nbsp;&nbsp;&nbsp;&nbsp;Sitemap&nbsp;&nbsp;&nbsp;&nbsp;Terms of Use</p>
                    </div>
                </div>
            </div>
        </section>


        <script>
            function myFunction() {
                var x = document.getElementById("myTopnav");
                if (x.className === "topnav") {
                    x.className += " responsive";
                } else {
                    x.className = "topnav";
                }
            }
        </script> 



        <script type="text/javascript">
            function ValidateForm(frm) {
                if (frm.Name.value == "") {
                    alert('Name is required.');
                    frm.Name.focus();
                    return false;
                }
                if (frm.FromEmailAddress.value == "") {
                    alert('Email address is required.');
                    frm.FromEmailAddress.focus();
                    return false;
                }
                if (frm.FromEmailAddress.value.indexOf("@") < 1 || frm.FromEmailAddress.value.indexOf(".") < 1) {
                    alert('Please enter a valid email address.');
                    frm.FromEmailAddress.focus();
                    return false;
                }
                if (frm.Comments.value == "") {
                    alert('Please enter comments or questions.');
                    frm.Comments.focus();
                    return false;
                }
                return true;
            }
        </script>

    </body>
</html>
